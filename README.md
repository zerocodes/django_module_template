# 🚀 Simpel Starter app template !

Template untuk membuat Aplikasi Django (Package) dari awal.

## Cara Pakai

Install cookie cutter

```
pip install cookiecutter
```

Buat project baru

```
cookiecutter https://gitlab.com/zerocodes/django_module_template.git
# isi data project sampai selsesai
```

setelah selesai, buka direktori project..
