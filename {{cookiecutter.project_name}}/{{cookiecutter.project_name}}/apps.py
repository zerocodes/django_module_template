from django.apps import AppConfig
from django.conf import settings
from django.db.models.signals import post_migrate
from django.utils.translation import gettext_lazy as _


class {{cookiecutter.project_config_name}}Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = "{{cookiecutter.project_name}}"
    label = "{{cookiecutter.project_label}}"
    verbose_name = _("{{cookiecutter.project_verbose_name}}")

    def ready(self):
        post_migrate.connect(init_app, sender=self)


def init_app(sender, **kwargs):
    pass
